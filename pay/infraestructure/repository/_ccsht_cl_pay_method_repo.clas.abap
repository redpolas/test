CLASS /ccsht/cl_pay_method_repo DEFINITION PUBLIC CREATE PRIVATE.
  PUBLIC SECTION.
    INTERFACES /ccsht/cl_pay_method_repo.

    ALIASES:
      type_e_hotel  FOR /ccsht/if_pay_method~type_e_hotel,
      type_e_id     FOR /ccsht/if_pay_method~type_e_id,
      type_e_name   FOR /ccsht/if_pay_method~type_e_name.

    CLASS-METHODS:
      create
        IMPORTING
          iv_hotel    TYPE type_e_hotel
          iv_language TYPE sy-langu DEFAULT sy-langu
        RETURNING
          VALUE(rr_repo) TYPE REF TO /ccsht/if_pay_method_repo.

  PRIVATE SECTION.
    TYPES:
      BEGIN OF type_s_cache,
        id          TYPE type_e_id,
        pay_method  TYPE REF TO /ccsht/if_pay_method,
      END OF type_s_cache,
      type_t_cache TYPE SORTED TABLE OF type_s_cache
                   WITH UNIQUE KEY id,
      BEGIN OF type_s_pay_method_data,
        id     TYPE type_e_id,
        name   TYPE type_e_name,
      END OF type_s_pay_method_data.

    DATA:
      _hotel TYPE type_e_hotel,
      _cache TYPE type_t_cache,
      _langu TYPE sy-langu.

    METHODS:
      constructor
        IMPORTING
          iv_language TYPE sy-langu
          iv_hotel    TYPE type_e_hotel,
      _obtain_pay_method_from_memory
        IMPORTING
          iv_id    TYPE type_e_id
        RETURNING
          VALUE(rr_pay_method) TYPE REF TO /ccsht/if_pay_method,
      _select_by_id
        IMPORTING
          iv_id    TYPE type_e_id
        RETURNING
          VALUE(rs_pay_method) TYPE type_s_pay_method_data
        RAISING
          /ccsht/cx_pay_method_repo,
      _select_all
        RETURNING
          VALUE(rt_pay_method) TYPE type_e_pay_method_data
        RAISING
          /ccsht/cx_pay_method_repo,
      _map_to_object
        IMPORTING
          is_pay_method TYPE type_s_pay_method_data
        RETURNING
          VALUE(rr_pay_method) TYPE REF TO /ccsht/if_pay_method,
      _register_pay_method_to_memory
        IMPORTING
          ir_pay_method TYPE REF TO /ccsht/if_pay_method,
      _unregister_pay_method_from_memory
        IMPORTING
          ir_pay_method TYPE REF TO /ccsht/if_pay_method.

ENDCLASS.

CLASS /ccsht/cl_pay_method_repo IMPLEMENTATION.
  METHOD create.
    rr_repo = NEW /ccsht/cl_pay_method_repo( iv_hotel    = iv_hotel
                                             iv_language = iv_language ).                                           
  ENDMETHOD.

  METHOD constructor.
    _hotel = iv_hotel.
    _langu = iv_language.
  ENDMETHOD.

  METHOD /ccsht/if_pay_method~find_by_id.
    rr_pay_method = _obtain_pay_method_from_memory( iv_id ).
    IF rr_pay_method IS NOT BOUND.
      rr_pay_method = _map_to_object( _select_by_id( iv_id ) ).
      _register_pay_method_to_memory( rr_pay_method ).
    ENDIF.
  ENDMETHOD.

  METHOD /ccsht/if_pay_method~find_all.
    rr_pay_methods = /ccsht/cl_pay_method=>create( ).
    LOOP AT _select_all( ) ASSIGNING FIELD-SYMBOL(<wa_pay_method_data>).
      DATA(lr_pay_method) = _obtain_pay_method_from_memory( <wa_pay_method_data>-id ).
      IF lr_pay_method IS NOT BOUND.
        lr_pay_method = _map_to_object( <wa_pay_method_data> ).
        _register_pay_method_to_memory( lr_pay_method ).
      ENDIF.
      rr_pay_methods->add_pay_method( lr_pay_method ).
    ENDLOOP.
  ENDMETHOD.

  METHOD _obtain_pay_method_from_memory.
    TRY.
        rr_pay_method = _cache[ id = iv_id ].
      CATCH cx_sy_itab_line_not_found.
    ENDTRY.
  ENDMETHOD.

  METHOD _select_by_id.

    SELECT SINGLE pay_method~xcodigo_fc_id     AS id,
                  pay_method_name~xdescripcion AS name
      FROM /CCSHT/MT_FCOBRO AS pay_method
        LEFT JOIN /CCSHT/MT_FCOBRO_t AS pay_method_name
          ON pay_method_name~xhotel_id      = pay_method~xhotel_id      AND
             pay_method_name~xcodigo_fc_id  = pay_method~xcodigo_fc_id  AND
             pay_method_name~xidioma   = @_langu
      INTO @rs_pay_method
     WHERE pay_method~xhotel_id      = @_hotel AND
           pay_method~xcodigo_fc_id  = @iv_id.
    IF sy-subrc <> 0.
      RAISE EXCEPTION TYPE /ccsht/cx_pay_method_repo
        EXPORTING textid = /ccsht/cx_pay_method_repo=>pay_method_not_exists
                iv_hotel = VALUE #( _hotel )
                   iv_id = VALUE #( iv_id ).
    ENDIF.
    
  ENDMETHOD.

  METHOD _select_all.
  "Test repo
    SELECT pay_method~xcodigo_fc_id     AS id,
           pay_method_name~xdescripcion AS name
      FROM /CCSHT/MT_FCOBRO AS pay_method
        LEFT JOIN /CCSHT/MT_FCOBRO_t AS pay_method_name
          ON pay_method_name~xhotel_id      = pay_method~xhotel_id      AND
             pay_method_name~xcodigo_fc_id  = pay_method~xcodigo_fc_id  AND
             pay_method_name~xidioma   = @_langu
      INTO TABLE @rt_pay_method
     WHERE pay_method~xhotel_id = @_hotel.
  ENDMETHOD.

  METHOD _map_to_object.

    rr_pay_method ?= NEW /ccsht/cl_pay_method( iv_hotel  = _hotel
                                               iv_id     = is_pay_method-id
                                               iv_name   = is_pay_method-name ).

  ENDMETHOD.

  METHOD _register_pay_method_to_memory.
    TRY.
        _cache[ id = ir_pay_method->id( ) ]-pay_method = ir_pay_method.
      CATCH cx_sy_itab_line_not_found.
        INSERT VALUE #( id    = ir_pay_method->id( )
                        pay_method  = ir_pay_method ) INTO TABLE _cache.
    ENDTRY.
  ENDMETHOD.

  METHOD _unregister_pay_method_from_memory.
    DELETE _cache WHERE id = ir_pay_method->id( ).
  ENDMETHOD.
ENDCLASS.